package le.tafb.utils.helpers;

import le.tafb.pages.HomePage;
import le.tafb.pages.LoginPage;
import le.tafb.utils.Browser;
import le.tafb.utils.data.DataHolder;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A helper class, contains methods like goToSpecificPage().
 */
public class Nav {
    private static final Logger logger = LoggerFactory.getLogger(Nav.class);

    /**
     * Navigates to the URL specified.
     *
     * @param url - URL to navigate to.
     */
    public static void toURL(String url){
        logger.debug("Navigating to " + url);
        Browser.getDriver().get(url);
    }

    /**
     * Performs logging in as an admin.
     * Note: it is supposed that current page is Login(Index) page.
     *
     * TODO: Add check for currentPage=LoginPage, Nav to Login if not.
     * @return HomePage object.
     */
    public static HomePage loginAsAdmin() {
        logger.debug("Logging in as admin.");
        LoginPage loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);

        return loginPage.loginAs(DataHolder.getHolder().getAdminName(), DataHolder.getHolder().getAdminPassword());
    }
}