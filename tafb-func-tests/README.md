Проект автотестов, часть учебной "песочницы" по автоматизации тестирования.
Тестируемое приложение - TAFB_AUT, .war-file лежит в загрузках(https://bitbucket.org/L_E_/tafb-func-tests/downloads).

Состоит из основных частей: Java (/src/test/java ...) и ресурсы (/src/test/resources).

/resources - содержит конфигурационные файлы и файлы свойств, а так же набор XML файлов используемых для наполнения БД.

/java - содержит пакеты:
    pages - классы типа PageObject моделирующие страницы тестируемого приложения. Класс BasePage - предполагается как родительский для всех классов PageObject. Содержит общие поля и сервисы, такие как logout() или ссылки из меню сверху всех страниц;
    tests - собственно автотесты. Подпакеты названы в соответствии с тестируемыми функциональными областями приложения. Все классы тестов наследуются  либо от AbstractTest(общие пред-/пост-условия, например запуск/останов браузера) или AbstractAutoLoginTest(включает заход в систему под учёткой админа в качестве предусловия);
    utils - вспомогательные классы (доступ к БД, аналоги тестируемых сущностей и т.д.). Названия подпакетов соответствуют функциональному предназначению утилитных классов.

==================================================

This test automation project contains 2 main parts: Java code(/src/test/java ...) and resources(/src/test/resources).

/resources
    Contains configuration and property files and data-sets as well. A data-set file is an DbUnit's XML data-set file used for data-base manipulations.

/java
    There are packages:
        pages - contains PageObjects for the AUT's pages. BasePage is intended to be a super class for all PageObjects in the project. Contains common fields and methods like "logout()".;
        tests - automated tests themselves. Contain sub-packages named after corresponding functional areas. All test classes extend either AbstractTest(common pre-/post-conditions like launch/close browser) or AbstractAutoLoginTest(performs logging in as an administrator as a pre-condition step.);
        utils - utility classes, helpers etc. There is a number of sub-packages which naming corresponds to the utility services their content provide. Refer to classes' JavaDocs.